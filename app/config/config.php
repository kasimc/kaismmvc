<?php
  // DB PARAMS
  define('DB_HOST', 'localhost');
  define('DB_USER', 'root');
  define('DB_PASS', 'root');
  define('DB_NAME', 'kmvc');

  // APP ROOT
  define('APPROOT', dirname(dirname(__FILE__)));
  // URL ROOT
  define('URLROOT', 'http://localhost/kasimmvc');
  // SITE NAME
  define('SITENAME', 'KaismMVC');
