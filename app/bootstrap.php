<?php

  // Load Config
  require_once 'config/config.php';

  // Autoload Library classes
  spl_autoload_register(function($classname) {
    require_once 'libraries/' . $classname . '.php';
  });
