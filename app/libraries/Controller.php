<?php
  /*
   * Base Controller
   * Load models and views
   */
  class Controller
  {

    // Load Models
    public function model($model)
    {
      // Require model class
      require_once '../app/models/'. $model . '.php';

      // Instantiate and retrun the model class
      return new $model;
    }

    // Load View
    public function view($view, $data = [])
    {
      // Checkif view exists
      if (file_exists('../app/views/' . $view . '.php')) {
        require_once '../app/views/' . $view . '.php';
      } else {
        die('View does not exists');
      }
    }
  }
