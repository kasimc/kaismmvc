<?php
  /*
   * APP Core Class
   * Creates URL & Load Controllers
   * URL FORMAT - /controller/method/params
   */
  class Core
  {

    protected $currentController = 'Pages';
    protected $currentMethod = 'index';
    protected $params = [];

    public function __construct()
    {
      // print_r($this->getUrl());
      $url = $this->getUrl();

      // Look in controllers for respective controller
      if (file_exists('../app/controllers/' . ucwords($url[0]) . '.php')) {
        // set current controller
        $this->currentController = ucwords($url[0]);
        unset($url[0]);
      }

      // Require the current controller
      require_once '../app/controllers/' . $this->currentController . '.php';

      // Instantiate controller class
      $this->currentController = new $this->currentController;

      if (isset($url[1])) {
        //Look for the Method
        if (method_exists($this->currentController, $url[1])) {
          // Set current Mehtod
          $this->currentMethod = $url[1];
          unset($url[1]);
        }
      }

      // Get the Params for the method
      $this->params = $url ? array_values($url) : [];

      // Call a callback with array of params
      call_user_func_array(
        [
          $this->currentController,
          $this->currentMethod
        ],
        $this->params
      );
    }

    public function getUrl()
    {
      if (isset($_GET['url'])) {
        $url = rtrim($_GET['url'], '/');
        $url = filter_var($url, FILTER_SANITIZE_URL);
        $url = explode('/', $url);
        return $url;
      }
    }
  }