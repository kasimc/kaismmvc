<?php
  /*
   * PDO Database Connection
   */
  class Database 
  {

    private $host = DB_HOST;
    private $user = DB_USER;
    private $pass = DB_PASS;
    private $dbname = DB_NAME;

    private $dbh;
    private $stmt;
    private $error;

    public function __construct()
    {
      // Create DNS for PDO connection
      $dns = "mysql:host=" . $this->host . ";dbname=" . $this->dbname;
      $options = array(
        PDO::ATTR_PERSISTENT => true,
        PDO::ATTR_ERRMODE => PDO::ERRMODE_EXCEPTION
      );

      try {
        // Create PDO instance
        $this->dbh = new PDO($dns, $this->user, $this->pass);
      } catch (PDOException $e) {
        $this->error = $e->getMessage();
        echo $this->error;
      }
    }

    // Prepared a query
    public function query($sql)
    {
      $this->stmt = $this->dbh->prepare($sql);
    }

    // Bind Values
    public function bind($param, $value, $type = null)
    {
      if (is_null($type)) {
        switch (true) {
          case is_int($value):
            $type = PDO::PARAM_INT;
            break;

          case is_bool($value):
            $type = PDO::PARAM_BOOL;
            break;

          case is_null($value):
            $type = PDO::PARAM_NULL;
            break;

          default:
            $type = PDO::PARAM_STR;
            break;
        }
      }

      $this->stmt->bindValue($param, $value, $type);
    }

    // Execute prepare statement
    public function execute()
    {
      return $this->stmt->execute();
    }

    // Get result in array of objects
    public function resultSet()
    {
      $this->execute();
      return $this->stmt->fetchAll(PDO::FETCH_OBJ);
    }

    // Get a single record in object
    public function singleResult()
    {
      $this->execute();
      return $this->stmt->fetch(PDO::FETCH_OBJ);
    }

    // Get the row count
    public function rowCount()
    {
      return $this->stmt->rowCount();
    }
  }